## Lili's tiny (or first test) blog

In this project, I combine React, Node.js, and Amazon Web Services (AWS) in a full-stack, full-featured website, including user-friendly forms for posting articles and comments. Helps me to learn how to create an interface from React components, develop a Node.js server, tie in a MongoDB database, and deploy my site (also future sites) on Amazon Web Services.

NOTE:  
This is not meant to be a perfectly designed, awesomely-looking app, as I am not focusing on the design itself in this project.

## Bootstrapped with

[Create React App](https://github.com/facebook/create-react-app)

## Stack used

React  
React Router  
React Hooks  
Node  
Express  
MongoDB  
Deployed to AWS (hopefully! :) )

Whatwg - polyfill library to make sure the app will work on IE  

