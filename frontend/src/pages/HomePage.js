import React from "react";

const HomePage = () => (
  <>
    <h1>Hello, welcome to Lili's blog</h1>
    <p>Full-stack Developer To Be!</p>
    <p>
      Currently in the project (practical) phase of the training, developing
      Tribes, an RPG game using ReactJS in the frontend, NodeJS, ExpressJS and
      MongoDB in the backend (MERN stack). Other technologies used: JavaScript,
      TypeScript, HTML, CSS, Git, GitHub, Mongoose, Slack for communication,
      Jira for work planning, using Scrum/Agile methodologies.
    </p>
  </>
);

export default HomePage;
